
import { Fragment, useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';

/*for React Context*/
import { UserProvider } from './UserContext';

/*components*/
import AppNavbar from './components/AppNavbar';

/*pages*/
import Home from './pages/Home';
import Course from './pages/Course';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';

export default function App() {

  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();

    setUser({
      id: null,
      isAdmin: null
    })
  }


  useEffect( () => {

    fetch("http://localhost:4000/api/users/details", {
      headers:{
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(response => response.json())
    .then(data => {
        console.log(data)

          if(data._id !== "undefined"){
            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            })
          }
    })

  }, [])



  return(
    <UserProvider value={ {user, setUser, unsetUser} }>
      <BrowserRouter>
        <AppNavbar/>
        <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/course" component={Course}/>
              <Route exact path="/register" component={Register}/>
              <Route exact path="/login" component={Login}/>
              <Route exact path="/logout" component={Logout}/>
              <Route exact path="*" component={NotFound} />
        </Switch>
      </BrowserRouter>
    </UserProvider>
  )
}

