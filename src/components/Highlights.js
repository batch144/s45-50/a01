
export default function Highlights(){

	return(

		<div className="container-fluid m-3">
        <div className="row mb-3">

         {/*card 1*/}
            <div className="col-10 col-md-4">
            	 <div class="card">
                 <div class="card-body">
                   <h5 class="card-title">Learn from Home</h5>
                   <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                   <a href="#" class="btn btn-primary">Go somewhere</a>
                 </div>
               </div>
            </div>

          {/*card 2*/}
               <div className="col-10 col-md-4">
                 <div class="card">
                    <div class="card-body">
                      <h5 class="card-title">Study Now, Pay Later</h5>
                      <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                       consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                       cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                       proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                      <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                  </div>
               </div>

             {/*card 3*/}
            <div className="col-10 col-md-4">
               <div class="card">
                 <div class="card-body">
                   <h5 class="card-title">Be Part of Our Community</h5>
                   <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                   <a href="#" class="btn btn-primary">Go somewhere</a>
                 </div>
               </div>
            </div>

        </div>
    </div>

		)
}