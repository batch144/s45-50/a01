
import { Fragment, useEffect, useState, useContext } from 'react';
import UserContext from './../UserContext'
import courseData from './../data/courseData';

/*components*/
	/*course card is the template for course*/
import CourseCard from './../components/CourseCard';

export default function Course(){

	const [course, setCourse] = useState([])

	const {user} = useContext(UserContext)

	// console.log(courseData)

	// const course = courseData.map( element => {
	// 	return (
	// 		<CourseCard key={element.id} courseProp={element}/>
	// 	)
	// })

	const fetchData = () => {
		fetch("http://localhost:4000//api/courses")
		.then(response => response.json())
		.then(data => {
			console.log(data)

			setCourse(data)
		})
	}

	useEffect( () => {
		fetchData()
	}, [])

	return(
		<Fragment>
			{

			}
		</Fragment>

	)
}