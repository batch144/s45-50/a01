import {Fragment} from 'react'

import 'bootstrap/dist/css/bootstrap.min.css';

import Banner from './../components/Banner';


export default function NotFound(){

	const data = {
		title: "404 - Not Found",
		content: "The page you are looking dor cannot be found.",
		destination: "/",
		label: "Back home"
	}

	return(
		<Fragment>
			<Banner data={data}/>
		</Fragment>

		)
}