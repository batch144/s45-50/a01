import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import Swal from "sweetalert2"

import { Redirect } from 'react-router-dom'
import UserContext from './../UserContext'

export default function Register(){

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [cpw, setCpw] = useState("")
	const [fname, setFname] = useState("")
	const [lname, setLname] = useState("")
	const [num, setNum] = useState("")

	const [isDisabled, setIsDisabled] = useState();

	useEffect( () => {
    	if(email !== "" && password !== "" && cpw !== "" && password === cpw){
      		setIsDisabled(false)
    	} else {
    		setIsDisabled(true)
    	}
  	}, [email, password, cpw, fname, lname, num] )

  	function Register(e){
  		e.preventDefault()

  		// alert("Registered Successfully!")

  		fetch("http://localhost:4000/api/users/register", {
  			method: "POST",
  			headers: {
  				"Content-Type": "application/json"
  			},
  			body: JSON.stringify({
  				email: email,
  				password: password
  			})
  		})
  		.then(response => response.json())
  		.then(data => {

  			//check if data is undefined or not
  			if(data !== "undefined"){

  				//store data in local storage
  				localStorage.setItem("token", data.access)

  				//invoke the function to retive user details
  				checkEmail(data.access)

  				//alert the user that login is successful
  				Swal.fire({
  					title: "Successfully Registered!",
  					icon: "success",
  					text: "Welcome to Zuitt!"
  				})
  			} else{
  				//alert the user that login failed
  				Swal.fire({
  					title: "Duplicate Email Found",
  					icon: "error",
  					text: "Please provide different email!"
  				})
  			}
  		})

  		setEmail("")
  		setPassword("")
  		setCpw("")
  		setFname("")
  		setLname("")
  		setNum("")
  	}


  	const checkEmail = (token) => {

  		//send request to the server
  		fetch("http://localhost:4000/api/users/email-exists", {
  			headers: {
  				"Authorization": `Bearer ${token}`
  			}
  		})
  		.then(response => response.json())
  		.then(data => {

  			//use setUser() to update the state
  			setUser({
  				id: data._id
  			})
  		})
  	}


	return(
		
		<Container fluid className="m-3">
		<h1>Register</h1>
			<Form className="border p-3 my-3" onSubmit={ (e) => Register(e) }>
		{/*firstname*/}
			  <Form.Group className="mb-3" controlId="fname">
			    <Form.Label>First Name</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter First Name" 
			    	value={fname}
			    	onChange={ (e) => setFname(e.target.value) } />
			  </Form.Group>
		{/*lastname*/}
			  <Form.Group className="mb-3" controlId="lname">
			    <Form.Label>Last Name</Form.Label>
			    <Form.Control 
			    	type="tex" 
			    	placeholder="Enter Last Name" 
			    	value={lname}
			    	onChange={ (e) => setEmail(e.target.value) } />
			  </Form.Group>
		{/*email*/}
			  <Form.Group className="mb-3" controlId="email">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value={email}
			    	onChange={ (e) => setEmail(e.target.value) } />
			  </Form.Group>
		{/*mobile no*/}
			  <Form.Group className="mb-3" controlId="num">
			    <Form.Label>Mobile number</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter Mobile number" 
			    	value={num}
			    	onChange={ (e) => setEmail(e.target.value) } />
			  </Form.Group>
		{/*password*/}
			  <Form.Group className="mb-3" controlId="password">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password}
			    	onChange={ (e) => setPassword(e.target.value) } />
			  </Form.Group>
		{/*confirm password*/}
				<Form.Group className="mb-3" controlId="cpw">
				  <Form.Label>Verify Password</Form.Label>
				  <Form.Control 
				  	type="password" 
				  	placeholder="Verify Password" 
				  	value={cpw}
				  	onChange={ (e) => setCpw(e.target.value) } />
				</Form.Group>
			  <Button variant="primary" type="submit" disabled={isDisabled}>
			    Submit
			  </Button>
			</Form>
		</Container>
	)
}